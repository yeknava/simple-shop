<?php

use Yeknava\SimpleShop\SampleSimpleShopObserver;

return [
    // table names
    'shops_table' => 'shops',

    'products_table' => 'products',

    'carts_table' => 'carts',

    'orders_table' => 'orders',

    'order_items_table' => 'order_items',

    'shipping_methods_table' => 'shipping_methods',

    'tracking_code_length' => 10,

    'observers' => [
        SampleSimpleShopObserver::class
    ],

    // use slugify library to automatically generate slugs based on title field
    'slugify' => true,

    'slugify_seperator' => '_',

    // list of rulesets => https://github.com/cocur/slugify/tree/master/Resources/rules
    'slugify_ruleset' => [
        'default',
    ],
];
