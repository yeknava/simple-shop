<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleShopOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-shop.orders_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('shop_id');
            $table->foreign('shop_id')
                ->references('id')
                ->on(config('simple-shop.shops_table'));
            $table->nullableMorphs('customer');
            $table->nullableMorphs('payment');
            $table->nullableMorphs('shipping');
            $table->string('name')->nullable();
            $table->string('currency', 6)->nullable();
            $table->string('status')->nullable();
            $table->double('amount', 16, 4)->unsigned()->default(0);
            $table->string('paid_by')->nullable();
            $table->double('paid_amount', 16, 4)->default(0);
            $table->double('amount_returned', 16, 4)->unsigned()->default(0);
            $table->text('description')->nullable();
            $table->smallInteger('shipping_by')->unsigned()->default(1);
            $table->text('shipping_address')->nullable();
            $table->text('shipping_zipcode')->nullable();
            $table->double('latitude', 9, 6)->nullable();
            $table->double('longitude', 9, 6)->nullable();
            $table->string('coupon')->nullable();
            $table->string('phone')->index()->nullable();
            $table->string('email')->index()->nullable();
            $table->string('receiver_name')->nullable();
            $table->string('tracking_code')->index()->nullable();
            $table->string('upstream_id')->index()->nullable();
            $table->jsonb('upstream_data')->nullable();
            $table->jsonb('shipping_data')->nullable();
            $table->jsonb('status_transitions')->nullable();
            $table->jsonb('extra')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->unique(['shop_id', 'upstream_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-shop.orders_table'));
    }
}
