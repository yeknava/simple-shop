<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleShopCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-shop.carts_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->nullableMorphs('owner');
            $table->bigInteger('shop_id')->nullable();
            $table->foreign('shop_id')
                ->references('id')
                ->on(config('simple-shop.shops_table'));
            $table->string('status')->nullable();
            $table->string('payment_type')->nullable();
            $table->double('total_amount', 16, 4)->default(0);
            $table->double('total_tax', 16, 4)->default(0);
            $table->double('total_shipping_amount')->default(0);
            $table->double('total_discount', 16, 4)->default(0);
            $table->text('shipping_address')->nullable();
            $table->jsonb('items')->nullable();
            $table->jsonb('extra')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-shop.carts_table'));
    }
}
