<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleShopShippingMethodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-shop.shipping_methods_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('shop_id');
            $table->foreign('shop_id')
                ->references('id')
                ->on(config('simple-shop.shops_table'));
            $table->string('title')->index();
            $table->text('description')->nullable();
            $table->string('type')->nullable();
            $table->smallInteger('min_width')->unsigned()->default(0);
            $table->smallInteger('max_width')->unsigned()->default(20);
            $table->smallInteger('min_height')->unsigned()->default(0);
            $table->smallInteger('max_height')->unsigned()->default(20);
            $table->double('min_weight', 6, 1)->unsigned()->default(0);
            $table->double('max_weight', 6, 1)->unsigned()->default(100.0);
            $table->double('price', 16, 4);
            $table->boolean('active')->default(true);
            $table->timestamp('expired_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-shop.shipping_methods_table'));
    }
}
