<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleShopProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-shop.products_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('shop_id');
            $table->foreign('shop_id')
                ->references('id')
                ->on(config('simple-shop.shops_table'));
            $table->bigInteger('parent_id')->nullable();
            $table->foreign('parent_id')
                ->references('id')
                ->on(config('simple-shop.shops_table'));
            $table->string('title')->index();
            $table->string('slug')->unique()->index();
            $table->string('category')->nullable();
            $table->string('type')->nullable();
            $table->string('color')->nullable();
            $table->string('status')->nullable();
            $table->integer('quantity')->nullable();
            $table->double('price', 16, 4);
            $table->double('sales_price', 16, 4);
            $table->double('tax', 16, 4)->default(0);
            $table->double('shipping_amount')->default(0);
            $table->smallInteger('shipping_type')->unsigned()->default(1);
            $table->smallInteger('width')->unsigned()->default(1);
            $table->smallInteger('height')->unsigned()->default(1);
            $table->text('image_path')->nullable();
            $table->text('short_description')->nullable();
            $table->longText('description')->nullable();
            $table->string('upstream_id')->index()->nullable();
            $table->boolean('standalone_shipping')->default(false);
            $table->boolean('active')->default(true);
            $table->boolean('is_finite')->default(true);
            $table->boolean('confirmation_needed')->default(false);
            $table->jsonb('tags')->nullable();
            $table->jsonb('extra')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->unique(['shop_id', 'upstream_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-shop.products_table'));
    }
}
