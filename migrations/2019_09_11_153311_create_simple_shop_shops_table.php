<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleShopShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-shop.shops_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->index();
            $table->nullableMorphs('owner');
            $table->string('slug')->unique();
            $table->string('key')->index()->nullable();
            $table->string('plan')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('is_default')->default(true);
            $table->longText('description')->nullable();
            $table->text('address')->nullable();
            $table->double('latitude', 9, 6)->nullable();
            $table->double('longitude', 9, 6)->nullable();
            $table->text('logo_path')->nullable();
            $table->jsonb('categories')->nullable();
            $table->jsonb('extra')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-shop.shops_table'));
    }
}
