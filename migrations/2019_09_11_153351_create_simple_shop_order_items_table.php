<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleShopOrderitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-shop.order_items_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->nullableMorphs('item');
            $table->bigInteger('order_id');
            $table->foreign('order_id')
                ->references('id')
                ->on(config('simple-shop.orders_table'));
            $table->string('currency', 6)->nullable();
            $table->string('type')->nullable();
            $table->double('amount', 16, 4)->unsigned()->default(0);
            $table->smallInteger('quantity')->unsigned()->nullable();
            $table->jsonb('customer_data')->nullable();
            $table->jsonb('extra')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-shop.order_items_table'));
    }
}
