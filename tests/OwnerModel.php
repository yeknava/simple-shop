<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleShop\SimpleShopOwner;
use Illuminate\Database\Eloquent\SoftDeletes;

class OwnerModel extends Model
{
    use SimpleShopOwner, SoftDeletes;

    protected $table = 'simple_shop_test_users';
}
