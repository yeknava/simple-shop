<?php

use Orchestra\Testbench\TestCase;
use Tests\OwnerModel;
use Tests\UserModel;
use Yeknava\SimpleShop\Exceptions\InvalidOrderStatusException;
use Yeknava\SimpleShop\Models\ProductModel;

class SimpleTest extends TestCase
{
    /**
     * Setup the test environment.
     */
    protected function setUp() : void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/../migrations', ['--database' => 'testing']);
        $this->loadMigrationsFrom(__DIR__ . '/migrations', ['--database' => 'testing']);
    }
    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testing');
        $app['config']->set('simple-shop', require_once(__DIR__ . '/../config/simple-shop.php'));
    }

    protected function getPackageProviders($app)
    {
        return [
            \Yeknava\SimpleShop\SimpleShopServiceProvider::class,
        ];
    }

    public function init()
    {
        $customer = (new UserModel([]));
        $customer->save();
        $shopOwner = (new OwnerModel([]));
        $shopOwner->save();

        $shopOwner->newShop('shop title');
        $this->assertEquals($shopOwner->shops()->count(), 1);
        
        $shop = $shopOwner->shop();
        $product = $shop->addProduct('product 1 title', 10000, null, [
            'description' => 'product 1 desc',
            'quantity' => 100,
            'active' => true,
            'type' => ProductModel::TYPE_GOOD
        ]);

        return [$customer, $shop, $product];
    }

    public function test()
    {
        [$customer, $shop, $product] = $this->init();

        $customer->addToCart($product, 90, ['color'=>'red']);
        $customer->addToCart($product, 1, ['color'=>'blue']);
        $this->assertEquals(2, count($customer->cart->items));
        
        $customer->addToCart($product, 10, ['color'=>'red']);
        $this->assertEquals(3, count($customer->cart()->first()->items));

        $customer->clearCart();
        $this->assertEquals(0, count($customer->cart()->first()->items));

        $customer->addToCart($product, 10, ['color'=>'red']);
        $this->assertEquals(1, count($customer->cart()->first()->items));

        $product = $shop->searchByTitle('product 1 title');
        $this->assertEquals('product 1 title', $product->title);
        $this->assertEquals(100, $product->quantity);
        
        $customer->cartPurchased();

        $product = $shop->searchByTitle('product 1 title');
        $this->assertEquals(90, $product->quantity);

        $customer->addToCart($product, 1, ['color'=>'red'], $shop->id);
        $this->assertEquals(1, $customer->cart($shop->id)->first()->items[0]['quantity']);

        $customer->addToCart($product, 10, ['color'=>'red']);
        $this->assertEquals(10, $customer->cart()->first()->items[0]['quantity']);

        $customer->cartOrdered($shop->id);

        $product2 = $shop->addProduct('product 2 title', 10000, null, [
            'description' => 'product 2 desc',
            'quantity' => 100,
            'active' => true,
            'type' => ProductModel::TYPE_GOOD
        ]);

        $order = $product->ordered(1, $customer, null, null, ['upstream_id' => 1234]);
        $order = $shop->findByUpstreamId(1234);
        $order->addProductItem($product2, 1);
        $this->assertEquals($order->amount, 20000);
        $this->assertEquals($product2->quantity, 100);
        $order->paid();
        $this->assertEquals($product2->refresh()->quantity, 99);
        $order->fulfilled();
        $order->returned();
        $this->assertEquals($product2->refresh()->quantity, 100);
        
        try {
            $order->canceled();
        } catch (InvalidOrderStatusException $e) {
            $this->assertEquals(true, true);
        }

        $product3 = $shop->addProduct('product 3 title', 10000, null, [
            'description' => 'product 3 desc',
            'quantity' => 100,
            'active' => true,
            'is_finite' => false,
            'type' => ProductModel::TYPE_SERVICE
        ]);

        $order = $product->ordered(1000, $customer, null, null, ['upstream_id' => 12345]);
        $order->paid();
        $this->assertEquals($product3->refresh()->quantity, 100);
        $order->fulfilled();
        $order->returned();
        $this->assertEquals($product3->refresh()->quantity, 100);
    }
}