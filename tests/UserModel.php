<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleShop\SimpleShopCustomer;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserModel extends Model
{
    use SimpleShopCustomer, SoftDeletes;

    protected $table = 'simple_shop_test_users';
}
