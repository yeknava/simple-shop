# Laravel Simple Shop Package

Laravel Simple Shop Package.

## Installation

Use the package manager [composer](https://getcomposer.org/) to install simple shop package.

```bash
composer require yeknava/simple-shop
```

## Usage
Run this command in your terminal:

```bash
php artisan vendor:publish
```

Add SimpleShopOwner trait to Shop owner models and Add SimpleShopCustomer trait to customer models if there is any.

```php
<?php

use Yeknava\SimpleShop\SimpleShopCustomer;

class User extends Model {
    use SimpleShopCustomer;
}
```

```php
<?php

use Yeknava\SimpleShop\SimpleShopOwner;

class OwnerModel extends Model {
    use SimpleShopOwner;
}
```

```php
$customer = (new UserModel([]));
$customer->save();
$shopOwner = (new OwnerModel([]));
$shopOwner->save();

$shopOwner->newShop('shop title');
$this->assertEquals($shopOwner->shops()->count(), 1);

$shop = $shopOwner->shop();
$product = $shop->addProduct('product 1 title', 10000, null, [
    'description' => 'product 1 desc',
    'quantity' => 100
]);

$customer->addToCart($product, 90, ['color'=>'red']);
$customer->addToCart($product, 1, ['color'=>'blue']);
$this->assertEquals(2, count($customer->cart->items));

$customer->addToCart($product, 10, ['color'=>'red']);
$this->assertEquals(3, count($customer->cart()->first()->items));

// clearing cart won't decrease product quantity
$customer->clearCart();
$this->assertEquals(0, count($customer->cart()->first()->items));

$customer->addToCart($product, 10, ['color'=>'red']);
$this->assertEquals(1, count($customer->cart()->first()->items));

$product = $shop->searchByTitle('product 1 title');
$this->assertEquals('product 1 title', $product->title);
$this->assertEquals(100, $product->quantity);

// purchasing cart will decrease product quantity
$customer->cartPurchased();

$product = $shop->searchByTitle('product 1 title');
$this->assertEquals(90, $product->quantity);

// shop specific cart instance for each customer 
$customer->addToCart($product, 1, ['color'=>'red'], $shop->id);
$this->assertEquals(1, $customer->cart($shop->id)->first()->items[0]['quantity']);

// default cart instance for each customer 
$customer->addToCart($product, 10, ['color'=>'red']);
$this->assertEquals(10, $customer->cart()->first()->items[0]['quantity']);
```

## Order Status Flow
success: Created -> Paid -> Fulfilled
failed: Created | Paid | Fulfilled -> Canceled
returned: Created -> Paid | Fulfilled -> Returned

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
