<?php

namespace Yeknava\SimpleShop;

use Yeknava\SimpleShop\Models\OrderModel;
use Yeknava\SimpleShop\Models\ProductModel;
use Yeknava\SimpleShop\Models\ShopModel;

class SampleSimpleShopObserver implements SimpleShopObserverInterface
{
    public function onNewShop(?string $title = null, ?string $slug = null, array $data = [])
    {
    }

    public function onNewShopSaved(ShopModel $shop)
    {
    }

    public function onNewProduct(array $data)
    {
    }

    public function onNewProductSaved(ProductModel $product)
    {
    }

    public function onAddCartItem(ProductModel $product, int $quantity = 1, array $options = [])
    {
    }

    public function onRemoveCartItem()
    {
    }

    public function onCartClear()
    {
    }

    public function onCartPurchase()
    {
    }

    public function onProductOrdered(ProductModel $product, OrderModel $order)
    {
    }

    public function onOrderCreated(OrderModel $order) {}

    public function onOrderPaid(OrderModel $order) {}

    public function onOrderCanceled(OrderModel $order) {}

    public function onOrderFulFilled(OrderModel $order) {}

    public function onOrderReturned(OrderModel $order) {}
}
