<?php

namespace Yeknava\SimpleShop\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yeknava\SimpleShop\Exceptions\NotActiveProductException;
use Yeknava\SimpleShop\Helper;

class ProductModel extends Model
{
    use SoftDeletes;

    const TYPE_GOOD = 'good';
    const TYPE_SERVICE = 'service';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('simple-shop.products_table');
    }

    protected $fillable = [
        'title',
        'slug',
        'category',
        'type',
        'status',
        'quantity',
        'price',
        'color',
        'tax',
        'sales_price',
        'shipping_type',
        'shipping_price',
        'standalone_shipping',
        'weight',
        'width',
        'height',
        'image_path',
        'short_description',
        'description',
        'upstream_id',
        'active',
        'is_finite',
        'confirmation_needed',
        'tags',
        'extra',
    ];

    protected $casts = [
        'paid_at' => 'datetime',
        'expired_at' => 'datetime',
        'extra' => 'array',
    ];

    protected $appends = ['discount'];

    public function getDiscountAttribute() : int
    {
        return $this->attributes['price'] - $this->attributes['sales_price'];
    }

    public function shop()
    {        
        return $this->belongsTo(ShopModel::class, 'shop_id');
    }

    public function parent()
    {        
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function orderItems()
    {
        return $this->morphMany(OrderItemModel::class, 'item');
    }

    public static function findByUpstreamId(int $shopId, string $upstreamId) :?self
    {
        return self::where('shop_id', $shopId)->where('upstream_id', $upstreamId)->first();
    }

    public function changeQuantity(int $quantity) : self {
        if (!$this->is_finite) return $this;
        
        $this->quantity += $quantity;
        $this->save();

        return $this;
    }

    public function ordered(
        int $quantity = null,
        Model $customer = null,
        Model $payment = null,
        Model $shipping = null,
        array $data = []
    ) :?OrderModel {
        if (!$this->active) {
            throw new NotActiveProductException();
        }
        $order = OrderModel::newOrder(
            $data,
            $this->shop,
            $customer,
            $payment,
            $shipping
        );
        $order->addProductItem($this, $quantity);
        Helper::Observe(Helper::ON_PRODUCT_ORDERED, $this, $order);

        return $order;
    }


}
