<?php

namespace Yeknava\SimpleShop\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yeknava\SimpleShop\Exceptions\DuplicateSlugException;
use Yeknava\SimpleShop\Exceptions\DuplicateUpstreamIdException;
use Yeknava\SimpleShop\Exceptions\EmptySlugException;
use Yeknava\SimpleShop\Helper;

class ShopModel extends Model
{
    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('simple-shop.shops_table');
    }

    protected $fillable = [
        'title',
        'description',
        'slug',
        'key',
        'plan',
        'logo_path',
        'is_default',
        'active',
        'categories',
        'extra',
        'expired_at'
    ];

    protected $casts = [
        'expired_at' => 'datetime',
        'extra' => 'array',
        'categories' => 'array',
    ];

    public function owner()
    {
        return $this->morphTo();
    }

    public function products()
    {
        return $this->hasMany(ProductModel::class, 'shop_id');
    }

    public function shippingMethods()
    {
        return $this->hasMany(ShippingMethodModel::class, 'shop_id');
    }

    public function orders()
    {
        return $this->hasMany(OrderModel::class, 'shop_id');
    }

    public function activeProducts()
    {
        return $this->hasMany(ProductModel::class, 'shop_id')->where('active', true);
    }

    public function deactiveProducts()
    {
        return $this->hasMany(ProductModel::class, 'shop_id')->where('active', false);
    }

    public function addProduct(
        string $title,
        float $price,
        string $slug = null,
        array $data
    ): ?ProductModel {
        if (!empty($data['upstream_id'])) {
            $product = self::where('shop_id', $this->id)
                ->where('upstream_id', $data['upstream_id'])
                ->first();

            if ($product) {
                throw new DuplicateUpstreamIdException();
            }
        }
        if (empty($slug) && config('simple-shop.slugify')) {
            $slug = Helper::slugGenerator($title);
        } elseif (empty($slug) && !config('simple-shop.slugify')) {
            throw new EmptySlugException();
        }

        if (ProductModel::where('slug', $slug)->first()) {
            throw new DuplicateSlugException();
        }

        Helper::Observe(Helper::ON_NEW_PRODUCT, $data);

        if (!isset($data['sales_price'])) {
            $data['sales_price'] = $price;
        }

        $product = new ProductModel(array_merge([
            'title' => $title,
            'slug' => $slug,
            'price' => $price,
        ], $data));
        $product->shop()->associate($this);
        $product->save();

        Helper::Observe(Helper::ON_NEW_PRODUCT_SAVED, $product);

        return $product;
    }

    public function deleteProduct(string $productId): bool
    {
        return $this->products()->where('id', $productId)->delete();
    }

    public function searchByTitle(string $title): ?ProductModel
    {
        return $this->products()->where('title', $title)->first();
    }

    public function searchBySlug(string $slug): ?ProductModel
    {
        return $this->products()->where('slug', $slug)->first();
    }

    public function findByUpstreamId(string $upstreamId): ?OrderModel
    {
        return $this->orders()->where('upstream_id', $upstreamId)->first();
    }
}
