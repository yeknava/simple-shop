<?php

namespace Yeknava\SimpleShop\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Yeknava\SimpleShop\Exceptions\DuplicateUpstreamIdException;
use Yeknava\SimpleShop\Exceptions\InvalidOrderAmountException;
use Yeknava\SimpleShop\Exceptions\InvalidOrderStatusException;
use Yeknava\SimpleShop\Exceptions\NotActiveProductException;
use Yeknava\SimpleShop\Helper;

class OrderModel extends Model
{
    use SoftDeletes;

    const STATUS_CREATED = 'created';
    const STATUS_CANCELED = 'canceled';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_PAID = 'paid';
    const STATUS_FULFILLED = 'fulfilled';
    const STATUS_RETURNED = 'returned';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('simple-shop.orders_table');
    }

    protected $fillable = [
        'currency',
        'amount',
        'amount_returned',
        'status',
        'coupon',
        'phone',
        'email',
        'tracking_code',
        'upstream_id',
        'upstream_data',
        'shipping_data',
        'expired_at',
        'paid_amount',
        'shipping_by',
        'paid_by',
        'description',
        'receiver_name',
        'latitude',
        'longitude',
    ];

    protected $casts = [
        'paid_at' => 'datetime',
        'expired_at' => 'datetime',
        'upstream_data' => 'array',
        'shipping_data' => 'array',
        'extra' => 'array',
        'status_transitions' => 'array'
    ];

    public function shop()
    {
        return $this->belongsTo(ShopModel::class, 'shop_id');
    }

    public function customer()
    {
        return $this->morphTo();
    }

    public function payment()
    {
        return $this->morphTo();
    }

    public function shipping()
    {
        return $this->morphTo();
    }

    public function items()
    {
        return $this->hasMany(OrderItemModel::class, 'order_id');
    }

    public function productItems()
    {
        return $this->items()->where('item_type', ProductModel::class);
    }

    public static function findByUpstreamId(int $shopId, string $upstreamId): ?self
    {
        return self::where('shop_id', $shopId)->where('upstream_id', $upstreamId)->first();
    }

    public static function newOrder(
        array $data,
        Model $shop,
        Model $customer = null,
        Model $payment = null,
        Model $shipping = null
    ): self {
        if (!empty($data['upstream_id'])) {
            $order = self::where('shop_id', $shop->id)
                ->where('upstream_id', $data['upstream_id'])
                ->first();

            if ($order) {
                throw new DuplicateUpstreamIdException();
            }
        }

        $order = new self($data);
        $order->status = self::STATUS_CREATED;
        $order->shop()->associate($shop);

        if (!empty($customer)) {
            $order->customer()->associate($customer);
        }

        if (!empty($payment)) {
            $order->payment()->associate($payment);
        }

        if (!empty($shipping)) {
            $order->shipping()->associate($shipping);
        }

        if (empty($data['tracking_code'])) {
            $order->tracking_code = bin2hex(random_bytes(config('simple-shop.tracking_code_length')));
        }

        $statusTransitions = [
            'canceled' => null,
            'paid' => null,
            'fulfilled' => null,
            'returned' => null,
        ];
        $order->status_transitions = $statusTransitions;
        $order->save();
        Helper::Observe(Helper::ON_ORDER_CREATED, $order);

        return $order;
    }

    public function addProductItem(
        ProductModel $product,
        int $quantity = null,
        array $data = []
    ): self {
        DB::beginTransaction();
        if (!$product->active) {
            throw new NotActiveProductException();
        }

        $orderItem = new OrderItemModel($data);
        $orderItem->amount = $product->sales_price;
        $orderItem->quantity = $quantity;
        $orderItem->type = OrderItemModel::TYPE_PRODUCT;
        $orderItem->item()->associate($product);
        $this->items()->save($orderItem);

        if ($quantity > 0) {
            $this->amount = (float)$this->amount + (float)$orderItem->amount * $quantity;
        } else {
            $this->amount = (float)$this->amount + (float)$orderItem->amount;
        }

        $this->save();

        DB::commit();

        return $this;
    }

    public function addItem(
        float $amount,
        string $type,
        int $quantity = null,
        array $data = []
    ): self {
        DB::beginTransaction();
        $orderItem = new OrderItemModel($data);
        $orderItem->amount = $amount;
        $orderItem->quantity = $quantity;
        $orderItem->type = $type;
        $this->items()->save($orderItem);

        if ($type == OrderItemModel::TYPE_DISCOUNT && $orderItem->amount > 0) {
            $this->amount = (float)$this->amount - (float)$orderItem->amount;
        } else {
            if ($quantity > 0) {
                $this->amount = (float)$this->amount + (float)$orderItem->amount * $quantity;
            } else {
                $this->amount = (float)$this->amount + (float)$orderItem->amount;
            }
        }

        $this->save();
        DB::commit();

        return $this;
    }

    public function changeProductQuantity(bool $minus = true): self
    {
        $productItems = $this->productItems()->with('item')->get();
        $keys = [];
        foreach ($productItems as $key => $productItem) {
            $product = $productItem->item;
            if (
                $product->type == ProductModel::TYPE_GOOD &&
                !empty($productItem->quantity) &&
                (int)$productItem->quantity > 0
            ) {
                if ($minus) {
                    $quantity = -$productItem->quantity;
                } else {
                    $quantity = $productItem->quantity;
                }
                $product->changeQuantity($quantity);
            }
            $keys[] = $productItem->quantity;
        }

        return $this;
    }

    public function paid(Model $payment = null, int $amount = null): self
    {
        if (!empty($amount) && $this->amount != $amount) {
            throw new InvalidOrderAmountException();
        }
        if (
            $this->status == self::STATUS_FULFILLED ||
            $this->status == self::STATUS_CANCELED ||
            $this->status == self::STATUS_PAID
        ) {
            throw new InvalidOrderStatusException();
        }
        DB::beginTransaction();

        if (!empty($payment)) {
            $this->payment()->associate($payment);
        }

        $this->status_transitions = array_merge($this->status_transitions, [
            'paid' => Carbon::now()
        ]);
        $this->status = self::STATUS_PAID;

        $this->save();

        $this->changeProductQuantity();

        Helper::Observe(Helper::ON_ORDER_PAID, $this);

        DB::commit();

        return $this;
    }

    public function canceled(): self
    {
        if (
            $this->status == self::STATUS_FULFILLED ||
            $this->status == self::STATUS_CANCELED
        ) {
            throw new InvalidOrderStatusException();
        }
        DB::beginTransaction();
        $this->status_transitions = array_merge($this->status_transitions, [
            'canceled' => Carbon::now()
        ]);
        $this->status = self::STATUS_CANCELED;
        $this->save();
        $this->changeProductQuantity(false);
        Helper::Observe(Helper::ON_ORDER_CANCELED, $this);

        DB::commit();

        return $this;
    }

    public function confirmed(): self
    {
        if (
            $this->status == self::STATUS_CANCELED ||
            $this->status == self::STATUS_FULFILLED
        ) {
            throw new InvalidOrderStatusException();
        }
        $this->status_transitions = array_merge($this->status_transitions, [
            'confirmed' => Carbon::now()
        ]);
        $this->status = self::STATUS_CONFIRMED;
        $this->save();
        Helper::Observe(Helper::ON_ORDER_CONFIRMED, $this);

        return $this;
    }

    public function fulfilled(): self
    {
        if (
            $this->status == self::STATUS_CANCELED ||
            $this->status == self::STATUS_FULFILLED
        ) {
            throw new InvalidOrderStatusException();
        }
        $this->status_transitions = array_merge($this->status_transitions, [
            'fulfilled' => Carbon::now()
        ]);
        $this->status = self::STATUS_FULFILLED;
        $this->save();
        Helper::Observe(Helper::ON_ORDER_FULFILLED, $this);

        return $this;
    }

    public function returned(float $amountReturned = null, array $items = []): self
    {
        if (
            $this->status == self::STATUS_CANCELED ||
            $this->status == self::STATUS_RETURNED
        ) {
            throw new InvalidOrderStatusException();
        }
        DB::beginTransaction();
        $this->status_transitions = array_merge($this->status_transitions, [
            'returned' => Carbon::now()
        ]);
        $this->extra = array_merge($this->extra ?? [], [
            'returned_items' => count($items) ? $items : $this->items
        ]);
        $this->amount_returned = $amountReturned ?? $this->amount;
        $this->status = self::STATUS_RETURNED;
        $this->save();
        $this->changeProductQuantity(false);
        Helper::Observe(Helper::ON_ORDER_RETURNED, $this);
        DB::commit();

        return $this;
    }
}
