<?php

namespace Yeknava\SimpleShop\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingMethodModel extends Model
{
    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('simple-shop.shipping_methods_table');
    }

    protected $fillable = [
        'title',
        'min_weight',
        'max_weight',
        'min_width',
        'max_width',
        'min_height',
        'max_height',
        'price',
        'type',
        'active',
        'expired_at',
    ];

    public function shop()
    {
        return $this->belongsTo(ShopModel::class, 'shop_id');
    }
}
