<?php

namespace Yeknava\SimpleShop\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Throwable;
use Yeknava\SimpleShop\CartItem;
use Yeknava\SimpleShop\Exceptions\InvalidQuantityException;
use Yeknava\SimpleShop\Exceptions\ProductNotFoundException;

class CartModel extends Model
{
    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('simple-shop.carts_table');
    }

    protected $fillable = [
        'status',
        'payment_type',
        'total_amount',
        'total_tax',
        'total_shipping_amount',
        'total_discount',
        'shipping_address',
        'items',
        'extra',
    ];

    protected $casts = [
        'extra' => 'array',
        'items' => 'array',
    ];

    public function owner()
    {
        return $this->morphTo();
    }

    public function shop()
    {
        return $this->belongsTo(ShopModel::class);
    }

    public function addItem(ProductModel $product, int $quantity = 1, $options = []) :self {
        try {
            DB::beginTransaction();

            $product = ProductModel::sharedLock()->find($product->id);
            
            if ($product->quantity - $quantity < 1) {
                throw new InvalidQuantityException();
            }
            
            $cartItem = new CartItem($product, $quantity);
            $cartItem->setOptions($options);
            $this->items = array_merge($this->items ?? [], [$cartItem->toArray()]);
            
            $amount = (double) $product->price * $quantity;
            $tax = (double) $product->tax * $quantity;

            $this->total_amount = (double) $this->total_amount + $amount;
            $this->total_tax = (double) $this->total_tax + $tax;
            $this->total_discount = (double) $this->total_discount + $product->discount;
            $this->total_shipping_amount = 
                (double) $this->total_shipping_amount + $product->shipping_amount;
            $this->save();

            DB::commit();
            
            return $this;
        } catch (Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function removeItem(int $itemKey, bool $isPurchase = false) : self {
        try {
            DB::beginTransaction();

            if (empty($this->items[$itemKey])) throw new ProductNotFoundException();

            $item = CartItem::fromCart($this->items[$itemKey]);
            $product = $item->getProduct();
            if ($isPurchase) $product->changeQuantity(-$item->getQuantity());
            $this->total_amount = (double) $this->total_amount - (double) $product->amount;
            $this->total_tax = (double) $this->total_tax - (double) $product->tax;
            $this->total_discount = (double) $this->total_discount - (double) $product->discount;
            $this->total_shipping_amount = 
                (double) $this->total_shipping_amount - (double) $product->shipping_amount;

            $items = $this->items;
            array_splice($items, $itemKey, 1);
            $this->items = $items;
            $this->save();

            DB::commit();

            return $this;
        } catch (Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function removeItems(array $itemKeys, bool $isPurchase = false) : self {
        try {
            DB::beginTransaction();

            $items = $this->items;
            foreach($itemKeys as $itemKey) {
                if (!is_numeric($itemKey) || empty($this->items[$itemKey])) {
                    throw new ProductNotFoundException();
                }

                $item = CartItem::fromCart($this->items[$itemKey]);
                $product = $item->getProduct();

                if ($isPurchase) $product->changeQuantity(-$item->getQuantity());

                $this->total_amount = (double) $this->total_amount - (double) $product->amount;
                $this->total_tax = (double) $this->total_tax - (double) $product->tax;
                $this->total_discount = (double) $this->total_discount - (double) $product->discount;
                $this->total_shipping_amount = 
                    (double) $this->total_shipping_amount - (double) $product->shipping_amount;

                unset($items[$itemKey]);
            }
            $this->items = array_values($items);
            $this->save();

            DB::commit();

            return $this;
        } catch (Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}
