<?php

namespace Yeknava\SimpleShop\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItemModel extends Model
{
    use SoftDeletes;

    const TYPE_PRODUCT = 'product';
    const TYPE_TAX = 'tax';
    const TYPE_DISCOUNT = 'discount';
    const TYPE_SHIPPING = 'shipping';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('simple-shop.order_items_table');
    }

    protected $fillable = [
        'extra',
        'customer_data',
        'currency',
        'type',
        'amount',
        'quantity',
        'title',
    ];

    protected $casts = [
        'customer_data' => 'json',
        'extra' => 'array',
    ];

    public function order()
    {
        return $this->belongsTo(OrderModel::class, 'order_id');
    }

    public function item()
    {
        return $this->morphTo();
    }

    public function product()
    {
        return $this->morphTo()->where('item_type', ProductModel::class);
    }
}
