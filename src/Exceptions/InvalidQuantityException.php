<?php

namespace Yeknava\SimpleShop\Exceptions;

class InvalidQuantityException extends SimpleShopException {
}