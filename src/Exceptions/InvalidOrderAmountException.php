<?php

namespace Yeknava\SimpleShop\Exceptions;

class InvalidOrderAmountException extends SimpleShopException {
}