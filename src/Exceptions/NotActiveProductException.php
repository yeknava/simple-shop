<?php

namespace Yeknava\SimpleShop\Exceptions;

class NotActiveProductException extends SimpleShopException {
}