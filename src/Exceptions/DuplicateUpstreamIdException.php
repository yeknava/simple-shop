<?php

namespace Yeknava\SimpleShop\Exceptions;

class DuplicateUpstreamIdException extends SimpleShopException {
}