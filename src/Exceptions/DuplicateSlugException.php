<?php

namespace Yeknava\SimpleShop\Exceptions;

class DuplicateSlugException extends SimpleShopException {
}