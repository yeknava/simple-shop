<?php

namespace Yeknava\SimpleShop\Exceptions;

class ProductNotFoundException extends SimpleShopException {
}