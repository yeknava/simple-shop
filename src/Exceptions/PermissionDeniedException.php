<?php

namespace Yeknava\SimpleShop\Exceptions;

class PermissionDeniedException extends SimpleShopException {
}