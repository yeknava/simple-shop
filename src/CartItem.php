<?php

namespace Yeknava\SimpleShop;

use Yeknava\SimpleShop\Exceptions\InvalidProductException;
use Yeknava\SimpleShop\Exceptions\ProductNotFoundException;
use Yeknava\SimpleShop\Models\ProductModel;

class CartItem
{
    private $product;
    private $quantity;
    private $options;

    public function __construct(ProductModel $product, int $quantity = 1)
    {
        $this->product = $product;
        $this->quantity = $quantity;
        $this->options = [];
    }

    public static function fromCart(array $data) : self
    {
        if (isset($data['product']['id'])) {
            $product = ProductModel::find($data['product']['id']);
            if (!$product) throw new InvalidProductException();
        } else {
            throw new ProductNotFoundException();
        }
        $self = new self($product, $data['quantity']);
        $self->setOptions($data['options']);

        return $self;
    }

    public function setProduct(ProductModel $product) : self
    {
        $this->product = $product;

        return $this;
    }

    public function setOptions(array $options) : self
    {
        $this->options = $options;

        return $this;
    }

    public function getProduct() : ProductModel
    {
        return $this->product;
    }

    public function getOptions() : array
    {
        return $this->options;
    }

    public function setQuantity(int $quantity) : self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getQuantity() : int
    {
        return $this->quantity;
    }

    public function toArray() : array
    {
        return [
            'product' => $this->product,
            'quantity' => $this->quantity,
            'options' => $this->options
        ];
    }
}
