<?php

namespace Yeknava\SimpleShop;

use Yeknava\SimpleShop\Exceptions\DuplicateSlugException;
use Yeknava\SimpleShop\Exceptions\EmptySlugException;
use Yeknava\SimpleShop\Models\ShopModel;

trait SimpleShopOwner {
    public function ownerShop(string $title = null): ?ShopModel
    {
        $shops = $this->ownerShops();
        
        if ($title) {
            $shops = $shops->where('title', $title);
        }

        return $shops->first();
    }

    public function ownerShops()
    {
        return $this->morphMany(ShopModel::class, 'owner');
    }

    public function createShop(
        string $title,
        string $slug = null,
        array $data = []
    ) :ShopModel {

        if (empty($slug) && config('simple-shop.slugify')) {
            $slug = Helper::slugGenerator($title);
        } elseif (empty($slug) && !config('simple-shop.slugify')) {
            throw new EmptySlugException();
        }

        if (ShopModel::where('slug', $slug)->first()) {
            throw new DuplicateSlugException();
        }

        Helper::Observe(Helper::ON_NEW_SHOP, $title, $slug, $data);

        $shop = new ShopModel(array_merge([
            'title' => $title,
            'slug' => $slug
        ], $data));
        $shop->owner()->associate($this);
        $shop->save();
        
        Helper::Observe(Helper::ON_NEW_SHOP_SAVED, $shop);

        return $shop;
    }
}