<?php

namespace Yeknava\SimpleShop;

use Illuminate\Support\Facades\DB;
use Throwable;
use Yeknava\SimpleShop\Exceptions\InvalidOrderStatusException;
use Yeknava\SimpleShop\Exceptions\PermissionDeniedException;
use Yeknava\SimpleShop\Models\CartModel;
use Yeknava\SimpleShop\Models\OrderItemModel;
use Yeknava\SimpleShop\Models\OrderModel;
use Yeknava\SimpleShop\Models\ProductModel;
use \Illuminate\Database\Eloquent\Model;


trait SimpleShopCustomer {
    public function customerCart(int $shopId = null)
    {
        if (!empty($shopId)) {
            return $this->morphOne(CartModel::class, 'owner')->where('shop_id', $shopId);
        }

        return $this->morphOne(CartModel::class, 'owner')->whereNull('shop_id');
    }

    public function customerCarts()
    {
        return $this->morphMany(CartModel::class, 'owner');
    }

    public function customerOrders()
    {
        return $this->morphMany(OrderModel::class, 'customer');
    }

    public function customerAddToCart(
        ProductModel $product,
        int $quantity = 1,
        array $options = [],
        int $shopId = null
    ) :CartModel {
        try {
            DB::beginTransaction();
            $cart = $this->cart($shopId)->sharedLock()->first();

            if (!$cart) {
                $cart = new CartModel();
                $cart->owner()->associate($this);
                if ($shopId) $cart->shop()->associate($shopId);
                $cart->save();
            }

            Helper::Observe(Helper::ON_ADD_CART_ITEM, $product, $quantity, $options);
            $cart->addItem($product, $quantity, $options);

            DB::commit();
            
            return $cart;
        } catch (Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function customerRemoveFromCart(int $itemKey, int $shopId = null) :CartModel {
        $cart = $this->cart($shopId)->sharedLock()->first();

        if (!$cart) {
            $cart = new CartModel();
            $cart->setOwner($this);
            if ($shopId) $cart->shop()->associate($shopId);
            $cart->save();

            return $cart;
        }

        Helper::Observe(Helper::ON_REMOVE_CART_ITEM);

        $cart->removeItem($itemKey);

        return $cart;
    }

    public function customerClearCart(int $shopId = null) :CartModel {
        $cart = $this->cart($shopId)->sharedLock()->first();

        if (!$cart) {
            $cart = new CartModel();
            $cart->setOwner($this);
            if ($shopId) $cart->shop()->associate($shopId);
            $cart->save();

            return $cart;
        }

        Helper::Observe(Helper::ON_CART_CLEAR);

        $cart->removeItems(array_keys($cart->items));

        return $cart;
    }

    public function customerCartOrdered(int $shopId) :CartModel {
        $cart = $this->cart($shopId)->sharedLock()->first();
        
        if (!$cart) {
            $cart = new CartModel();
            $cart->setOwner($this);
            if ($shopId) $cart->shop()->associate($shopId);
            $cart->save();

            return $cart;
        }

        $shop = $cart->shop;
        $order = $this->orders()
            ->where('shop_id', $shop->id)
            ->where('status', OrderModel::STATUS_CREATED)
            ->first();
        if (!$order) {
            $order = OrderModel::newOrder([], $shop, $this);
        }

        $orderProductItems = [];
        foreach ($order->items as $orderItem) {
            if (
                !empty($orderItem['item_id']) &&
                $orderItem['item_type'] == ProductModel::class
            ) {
                $orderProductItems[$orderItem['item_id']] = $orderItem->quantity;
            }
        }

        foreach ($cart->items as $item) {
            if (empty($orderProductItems[$item['product']['id']])) {
                $product = ProductModel::find($item['product']['id']);
                if ($product) $order->addProductItem($product, $item['quantity']);
            } elseif ($orderProductItems[$item['id']] != $item['quantity']) {
                OrderItemModel::where('order_id', $order->id)
                    ->where('item_id', $item['id'])
                    ->update(['quantity' => $item['quantity']]);
            }
        }
        
        return $cart;
    }

    public function customerCartPurchased(int $shopId = null) :CartModel {
        $cart = $this->cart($shopId)->sharedLock()->first();
        
        if (!$cart) {
            $cart = new CartModel();
            $cart->setOwner($this);
            if ($shopId) $cart->shop()->associate($shopId);
            $cart->save();

            return $cart;
        }

        Helper::Observe(Helper::ON_CART_PURCHASE);

        $cart->removeItems(array_keys($cart->items), true);

        return $cart;
    }

    public function customerGetLastShopOrder(int $shopId) :OrderModel {
        return $this->orders()
            ->where('shop_id', $shopId)
            ->whereIn('status', [
                OrderModel::STATUS_CREATED,
            ])
            ->orderBy('id', 'desc')
            ->first();
    }

    public function customerGetLastShopPaidOrder(int $shopId) :OrderModel {
        return $this->orders()
            ->where('shop_id', $shopId)
            ->whereIn('status', [
                OrderModel::STATUS_PAID,
            ])
            ->orderBy('id', 'desc')
            ->first();
    }

    public function customerPayOrder(
        int $orderId,
        ?Model $payment = null,
        ?int $amount = null
    ) :OrderModel {
        /** @var OrderModel */
        $order = $this->orders()->where('id', $orderId)->first();

        if (! $order) {
            return new PermissionDeniedException();
        }

        if (
            in_array($order->status, [
                OrderModel::STATUS_PAID,
                OrderModel::STATUS_FULFILLED,
                OrderModel::STATUS_RETURNED,
                OrderModel::STATUS_CANCELED,
            ])
        ) {
            return new InvalidOrderStatusException();
        }

        return $order->paid($payment, $amount);
    }

    public function customerFulfillOrder(int $orderId) :OrderModel {
        /** @var OrderModel */
        $order = $this->orders()->where('id', $orderId)->first();

        if (! $order) {
            return new PermissionDeniedException();
        }

        if ($order->status !== OrderModel::STATUS_PAID) {
            return new InvalidOrderStatusException();
        }
        
        return $order->fulfilled();
    }


    public function customerCancelOrder(int $orderId) :OrderModel {
        /** @var OrderModel */
        $order = $this->orders()->where('id', $orderId)->first();

        if (! $order) {
            return new PermissionDeniedException();
        }

        return $order->canceled();
    }

    public function customerReturnOrder(int $orderId,
        ?float $amountReturned = null,
        array $items = []
    ) :OrderModel {
        /** @var OrderModel */
        $order = $this->orders()->where('id', $orderId)->first();

        if (! $order) {
            return new PermissionDeniedException();
        }

        return $order->returned($amountReturned, $items);
    }

    public function customerFindOrderByTrackingCode(
        string $phoneOrEmail,
        string $trackingCode,
        bool $forceUserId = true,
    ): ?OrderModel {
        if ($forceUserId) {
            $query = $this->orders();
        } else {
            $query = OrderModel::query();
        }

        $query->where(function($q) use ($phoneOrEmail) {
            $q->where('phone', $phoneOrEmail)
                ->orWhere('email', $phoneOrEmail);
        });

        return $query->where('tracking_code', $trackingCode)->first();
    }

    public function customerSearchCartItemsByProductId(string $id, int $shopId = null) : ?array {
        $cart = $this->cart($shopId)->first();

        if (!$cart) return null;

        foreach($cart->items as $item) {
            if ($item->id === $id) return $item;
        }

        return null;
    }

    public function customerSearchCartItemsByProductTitle(string $title, int $shopId = null) : ?array {
        $cart = $this->cart($shopId)->first();

        if (!$cart) return null;

        foreach($cart->items as $item) {
            if ($item->title === $title) return $item;
        }

        return null;
    }

    public function customerSearchCartItemsByProductSlug(string $slug, int $shopId = null) : ?array {
        $cart = $this->cart($shopId)->first();

        if (!$cart) return null;

        foreach($cart->items as $item) {
            if ($item->slug === $slug) return $item;
        }

        return null;
    }
}