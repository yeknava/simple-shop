<?php

namespace Yeknava\SimpleShop;

use Illuminate\Support\ServiceProvider;

class SimpleShopServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            dirname(__DIR__, 1).'/config/simple-shop.php' =>
                $this->app->configPath().'/simple-shop.php',
                
            dirname(__DIR__, 1) . '/migrations/' =>
                database_path('migrations'),
        ]);
    }
}
