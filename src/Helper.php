<?php

namespace Yeknava\SimpleShop;

use Cocur\Slugify\Slugify;

class Helper
{
    const ON_NEW_SHOP = 'onNewShop';
    const ON_NEW_SHOP_SAVED = 'onNewShopSaved';
    const ON_NEW_PRODUCT = 'onNewProduct';
    const ON_NEW_PRODUCT_SAVED = 'onNewProductSaved';
    const ON_ADD_CART_ITEM = 'onAddCartItem';
    const ON_REMOVE_CART_ITEM = 'onRemoveCartItem';
    const ON_CART_CLEAR = 'onCartClear';
    const ON_CART_PURCHASE = 'onCartPurchase';
    const ON_PRODUCT_ORDERED = 'onProductOrdered';
    const ON_ORDER_CREATED = 'onOrderCreated';
    const ON_ORDER_CONFIRMED = 'onOrderConfirmed';
    const ON_ORDER_PAID = 'onOrderPaid';
    const ON_ORDER_CANCELED = 'onOrderCanceled';
    const ON_ORDER_FULFILLED = 'onOrderFulFilled';
    const ON_ORDER_RETURNED = 'onOrderReturned';

    public static function Observe(string $methodName, ...$args) {
        $observers = config('simple-shop.observers');

        foreach ($observers as $observer) {
            $observer = new $observer();
            if (method_exists($observer, $methodName)) {
                $observer->{$methodName}(...$args);
            }
        }
    }

    public static function slugGenerator(string $title) : string {
        $seperator = config('simple-shop.slugify_seperator');
        $slugify = new Slugify();
        $ruleset = config('simple-shop.slugify_ruleset');
        if (!count($ruleset)) {
            $slugify = $slugify->activateRuleSet($ruleset);
        }
        
        return $slugify->slugify($title, $seperator ?? '_');
    }
}
